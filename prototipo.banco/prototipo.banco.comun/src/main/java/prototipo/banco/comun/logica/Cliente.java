package prototipo.banco.comun.logica;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import prototipo.banco.comun.excepcion.PrototipoExcepcion;

import com.gigaspaces.annotation.pojo.SpaceId;
import com.gigaspaces.annotation.pojo.SpaceRouting;

/**
 * Clase que representa a un cliente
 * 
 * @author Oscar Ospina
 *
 */
@Entity
@Table(name = "clientes")
public class Cliente implements Serializable {

	/**
	 * El numero serial utilizado para serializar la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Numero de identificaci�n del cliente.
	 */
	private String numeroDeIdentificacion;

	/**
	 * Nombre del cliente.
	 */
	private String nombre;

	/**
	 * Direcci�n del cliente.
	 */
	private String direccion;

	/**
	 * Telefono del cliente.
	 */
	private String telefono;

	/**
	 * Lista de las cuentas del cliente.
	 */
	private List<Cuenta> cuentas;

	/**
	 * Contructor de un cliente sin datos.
	 */
	public Cliente() {
		cuentas = new ArrayList<Cuenta>();
	}

	/**
	 * Contructor de un cliente con todos los datos.
	 * 
	 * @param numeroDeIdentificacionNuevo - El numero del documento de identidad
	 *        del cliente
	 * @param nombreNuevo - El nombre completo del cliente
	 * @param direccionNueva - la direccion del cliente
	 * @param telefonoNuevo - el telefono fijo o celular del cliente
	 */
	public Cliente(String numeroDeIdentificacionNuevo, String nombreNuevo, String direccionNueva,
			String telefonoNuevo) {
		numeroDeIdentificacion = numeroDeIdentificacionNuevo;
		setNombre(nombreNuevo);
		setDireccion(direccionNueva);
		setTelefono(telefonoNuevo);
		cuentas = new ArrayList<Cuenta>();
	}

	/**
	 * Retorna el numero del documento de identidad del cliente.
	 * 
	 * @return numero del documento de identidad del cliente
	 */
	@Id
	@Column(name = "id", unique = true, nullable = false)
	@SpaceId
	@SpaceRouting
	public String getNumeroDeIdentificacion() {
		return numeroDeIdentificacion;
	}

	/**
	 * Establece un nuevo numero del documento de identidad del cliente.
	 * 
	 * @param numeroNuevo el numero nuevo
	 */
	public void setNumeroDeIdentificacion(String numeroNuevo) {
		numeroDeIdentificacion = numeroNuevo;
	}

	/**
	 * Retorna el nombre completo del cliente.
	 * 
	 * @return nombre completo del cliente
	 */
	@Column(name = "nombre")
	public String getNombre() {
		return nombre;
	}

	/**
	 * Establece el nombre completo del cliente.
	 * 
	 * @param nombre el nuevo nombre completo del cliente
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * Retorna la direccion del cliente.
	 * 
	 * @return direccion del cliente
	 */
	@Column(name = "direccion")
	public String getDireccion() {
		return direccion;
	}

	/**
	 * Establece la nueva direccion del cliente.
	 * 
	 * @param direccion la nueva direccion del cliente
	 */
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	/**
	 * Retorna el telefono del cliente.
	 * 
	 * @return telefono el numero telefonico del cliente
	 */
	@Column(name = "telefono")
	public String getTelefono() {
		return telefono;
	}

	/**
	 * Establece el nuevo telefono del cliente.
	 * 
	 * @param telefono el nuevo numero telefonico del client
	 */
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	/**
	 * Retorna todas las cuentas que el cliente tiene asociadas.
	 * 
	 * @return cuentas asociadas
	 */
	@OneToMany(mappedBy = "cliente", fetch = FetchType.EAGER)
	public List<Cuenta> getCuentas() {
		return cuentas;
	}

	/**
	 * Establece las cuentas del cliente.
	 * 
	 * @param cuentas lista con las cuentas del cliente
	 */
	public void setCuentas(List<Cuenta> cuentas) {
		this.cuentas = cuentas;
	}

	/**
	 * Asocia una cuenta al cliente.
	 * 
	 * @param cuenta la cuenta a asociar
	 */
	public void addCuenta(Cuenta cuenta) {
		cuentas.add(cuenta);
	}

	/**
	 * Elimina una cuenta asociada al cliente.
	 * 
	 * @param cuenta el numero de la cuenta que se desea eliminar
	 */
	public void deleteCuenta(Cuenta cuenta) {
		cuentas.remove(cuenta);
	}

	/**
	 * Registra un movimiento efectuado sobre una cuenta asociada del cliente.
	 * 
	 * @param numero el numero de la cuenta
	 * @param tipo el tipo de movimiento (d�bito o cr�dito)
	 * @param valor el monto por el cual se realiza el movimiento
	 * @throws Excepciones
	 */
	public void addMovimiento(Integer numero, Cuenta cuenta, String tipo, BigDecimal valor)
			throws PrototipoExcepcion {
		for (int i = 0; i < cuentas.size(); i++) {
			if (cuentas.get(i).getNumero().equals(cuenta.getNumero())) {
				cuentas.get(i).addMovimiento(
						new Movimiento(numero, tipo, new Date(new java.util.Date().getTime()),
								valor, cuenta));
			}
		}
	}

	/**
	 * Retorna una cuenta asociada con el cliente.
	 * 
	 * @param numeroCuenta el numero de la cuenta asociada
	 * @return la cuenta asociada
	 */
	public Cuenta getCuenta(int numeroCuenta) {
		Cuenta cuenta = null;
		for (int i = 0; i < cuentas.size(); i++) {
			if (cuentas.get(i).getNumero() == numeroCuenta) {
				cuenta = cuentas.get(i);
			}
		}
		return cuenta;
	}

	/**
	 * Actualiza los datos de un cliente.
	 * 
	 * @param nombreNuevo el nuevo nombre que se le asigna al cliente
	 * @param direccionNuevo es la nueva direccion del cliente
	 * @param telefonoNuevo es el nuevo telefono del cliente
	 */
	public void actualizar(String nombreNuevo, String direccionNuevo, String telefonoNuevo) {
		nombre = nombreNuevo;
		direccion = direccionNuevo;
		telefono = telefonoNuevo;
	}
}