package prototipo.banco.comun.logica;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import com.gigaspaces.annotation.pojo.SpaceId;
import com.gigaspaces.annotation.pojo.SpaceRouting;

/**
 * Clase que representa un movimiento
 * 
 * @author Oscar Ospina
 *
 */
@Entity
@Table(name = "movimientos")
public class Movimiento implements Serializable {

	/**
	 * El numero serial utilizado para serializar la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Numero del movimiento.
	 */
	private Integer numero;

	/**
	 * Tipo del movimiento.
	 */
	private TipoMovimiento tipo;

	/**
	 * Fecha del movimiento.
	 */
	private Date fecha;

	/**
	 * Valor del movimiento.
	 */
	private BigDecimal valor;

	/**
	 * Cuenta a la cual se le realiza el movimiento.
	 */
	private Cuenta cuenta;

	/**
	 * Indica si el movimiento ya fue procesado.
	 */
	private boolean procesado;

	/**
	 * Constructor de un movimiento sin datos.
	 */
	public Movimiento() {
		setProcesado(false);
	}

	/**
	 * Constructor de un movimiento con todos los datos.
	 * 
	 * @param tipoMovimiento el tipo de movimiento (d�bito o cr�dito)
	 * @param fechaMovimiento la fecha en la que se realizo el movimiento
	 * @param valorMovimiento el monto porel cual se realizo el movimiento
	 */
	public Movimiento(Integer numero, String tipoMovimiento, Date fechaMovimiento,
			BigDecimal valorMovimiento, Cuenta cuenta) {
		TipoMovimiento tipo = null;

		if (tipoMovimiento.equals("debito")) {
			tipo = TipoMovimiento.debito;
		} else if (tipoMovimiento.equals("credito")) {
			tipo = TipoMovimiento.credito;
		}
		setNumero(numero);
		setTipo(tipo);
		setFecha(fechaMovimiento);
		setValor(valorMovimiento);
		setCuenta(cuenta);

	}

	/**
	 * Retorna el numero con el que se identifica el movimiento.
	 * 
	 * @return numero con el que se identifica el movimiento
	 */
	@Id
	@Column(name = "numero")
	@SpaceId
	@SpaceRouting
	public Integer getNumero() {
		return numero;
	}

	/**
	 * Establece el numero con el que se identifica el movimiento.
	 * 
	 * @param numero el nuevo numero
	 */
	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	/**
	 * Retorna el tipo del movimiento (cr�dito o d�bito).
	 * 
	 * @return (cr�dito o d�bito)
	 */
	@Column(name = "tipo")
	@Enumerated(EnumType.STRING)
	public TipoMovimiento getTipo() {
		return tipo;
	}

	/**
	 * Establece el tipo del movimiento (cr�dito o d�bito).
	 * 
	 * @param tipo (cr�dito o d�bito)
	 */
	public void setTipo(TipoMovimiento tipo) {
		this.tipo = tipo;
	}

	/**
	 * Retorna la fecha en la que se efectuoel movimiento.
	 * 
	 * @return fecha en la que se efectuoel movimiento
	 */
	@Column(name = "fecha")
	@DateTimeFormat(pattern = "dd/mm/yyyy")
	public Date getFecha() {
		return fecha;
	}

	/**
	 * Establece la fecha en la que se efectuoel movimiento.
	 * 
	 * @param fecha en la que se efectuoel movimiento
	 */
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	/**
	 * Retorna el valor por el cual se realizo el movimiento.
	 * 
	 * @return valor por el cual se realizo el movimiento
	 */
	@Column(name = "valor")
	public BigDecimal getValor() {
		return valor;
	}

	/**
	 * Establece el valor por el cual se va realizar el movimiento.
	 * 
	 * @param valor monto por el cual se va realizar el movimiento
	 */
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	/**
	 * Retorna la cuenta a la cual se le efectu� el movimiento.
	 * 
	 * @return La cuenta a la cual se le efectu� el movimiento.
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "numero_cuenta", referencedColumnName = "numero")
	public Cuenta getCuenta() {
		return cuenta;
	}

	/**
	 * Establece la cuenta a la cual se le realiza el movimiento
	 * 
	 * @param cuenta que es afectada
	 */
	public void setCuenta(Cuenta cuenta) {
		this.cuenta = cuenta;
	}

	/**
	 * Retorna si el movimiento ya fue procesado o no.
	 * 
	 * @return
	 */
	@Column(name = "procesado")
	public boolean isProcesado() {
		return procesado;
	}

	/**
	 * 
	 * @param procesado
	 */
	public void setProcesado(boolean procesado) {
		this.procesado = procesado;
	}
}
