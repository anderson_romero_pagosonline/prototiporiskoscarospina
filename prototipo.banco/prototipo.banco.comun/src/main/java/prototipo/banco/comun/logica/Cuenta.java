package prototipo.banco.comun.logica;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.transaction.annotation.Transactional;

import prototipo.banco.comun.excepcion.PrototipoExcepcion;

import com.gigaspaces.annotation.pojo.SpaceId;
import com.gigaspaces.annotation.pojo.SpaceRouting;

/**
 * Clase que representa las cuentas de los clientes
 * 
 * @author Oscar Ospina
 *
 */
@Entity
@Table(name = "cuentas")
@Inheritance(strategy = InheritanceType.JOINED)
public class Cuenta implements Serializable {

	/**
	 * El numero serial utilizado para serializar la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Numero de la cuenta.
	 */
	private Integer numero;

	/**
	 * Saldo de la cuenta.
	 */
	private BigDecimal saldo;

	/**
	 * Lista de los movimientos de la cuenta.
	 */
	private Set<Movimiento> movimientos = new HashSet<Movimiento>();

	/**
	 * Cliente el cual es due�o de la cuenta.
	 */
	private Cliente cliente;

	/**
	 * Constructor de un cuenta sin datos.
	 */
	public Cuenta() {
	}

	/**
	 * Constructor de una cuenta con todos los datos.
	 * 
	 * @param Cliente es el due�o de la cuenta
	 * @param numeroNuevo es el numero con el cual se identifica la cuenta
	 * @param saldoNuevo es el dinero que tiene esta cuenta
	 */
	public Cuenta(Cliente clienteId, Integer numeroNuevo, BigDecimal saldoNuevo) {
		numero = numeroNuevo;
		setCliente(clienteId);
		setSaldo(saldoNuevo);
	}

	/**
	 * Retorna el numero con el que se identifica la cuenta.
	 * 
	 * @return numero con el que se identifica la cuenta
	 */
	@Id
	@Column(name = "numero", unique = true, nullable = false)
	@SpaceId
	@SpaceRouting
	public Integer getNumero() {
		return numero;
	}

	/**
	 * Establece el numero con el cual se identifica la cuenta.
	 * 
	 * @param numero con el que se identifica la cuenta
	 */
	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	/**
	 * Retorna el monto actual de la cuenta.
	 * 
	 * @return monto actual
	 */
	@Column(name = "saldo")
	public BigDecimal getSaldo() {
		return saldo;
	}

	/**
	 * Establece el saldoactual de la cuenta.
	 * 
	 * @param saldo el nuevo saldo de la cuenta
	 */
	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}

	/**
	 * Retorna todos los movimientos que se han realizado con la cuenta.
	 * 
	 * @return movimentos realizados con la cuenta
	 */
	@OneToMany(mappedBy = "cuenta", fetch = FetchType.EAGER)
	public Set<Movimiento> getMovimientos() {
		return movimientos;
	}

	/**
	 * Establece los movimiento realizados con la cuenta.
	 * 
	 * @param m sn los movimientos.
	 */
	public void setMovimientos(Set<Movimiento> m) {
		movimientos = m;
	}

	/**
	 * Retorna el cliente due�o de esta cuenta
	 * 
	 * @return el cliente due�o de esta cuenta
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_cliente", referencedColumnName = "id")
	public Cliente getCliente() {
		return cliente;
	}

	/**
	 * Establece el cliente due�o de esta cuenta
	 * 
	 * @param cliente es el due�o de esta cuenta
	 */
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	/**
	 * Agrega un movimiento a la cuenta.
	 * 
	 * @param movimiento el movimiento
	 * @throws Excepciones
	 */
	public void addMovimiento(Movimiento movimiento) throws PrototipoExcepcion {
		if (movimiento.getTipo().equals(TipoMovimiento.credito)
				&& saldo.doubleValue() < movimiento.getValor().doubleValue()) {
			throw new PrototipoExcepcion(
					"Esta operacion no es permitida la cuenta no puede tener un saldo negativo.");
		}
		modificarSaldo(movimiento);
		movimientos.add(movimiento);
	}

	/**
	 * Modifica el valor del saldo segun un movimiento.
	 * 
	 * @param movimiento es el movimiento
	 */
	public void modificarSaldo(Movimiento movimiento) {
		if (movimiento.getTipo().equals(TipoMovimiento.credito)) {
			saldo = saldo.subtract(movimiento.getValor());
		} else if (movimiento.getTipo().equals(TipoMovimiento.debito)) {
			saldo = saldo.add(movimiento.getValor());
		} else {
			;
		}
	}

	/**
	 * Calcula el total de los creditos de la cuenta entre dos fechas
	 * 
	 * @param fechaInicio la fecha inicial
	 * @param fechaFin la fecha fincal
	 * @return el valor total de los creditos realizados a esta cuenta
	 */
	public BigDecimal buscarCreditosFecha(Date fechaInicio, Date fechaFin) {
		BigDecimal credito = new BigDecimal(0);
		for (Movimiento movimiento : movimientos) {
			if (movimiento.getTipo().equals(TipoMovimiento.credito)) {
				credito = credito.add(movimiento.getValor());
			}
		}
		return credito;
	}

	/**
	 * Calcula el total de los debitos de la cuenta entre dos fechas
	 * 
	 * @param fechaInicio la fecha inicial
	 * @param fechaFin la fecha fincal
	 * @return el valor total de los debitos realizados a esta cuenta
	 */
	public BigDecimal buscarDebitosFecha(Date fechaInicio, Date fechaFin) {
		BigDecimal debito = new BigDecimal(0);
		for (Movimiento m : movimientos) {
			if (m.getTipo().equals(TipoMovimiento.debito) && m.getFecha().compareTo(fechaFin) < 0
					&& m.getFecha().compareTo(fechaInicio) > 0) {
				debito = debito.add(m.getValor());
			}
		}
		return debito;
	}
}