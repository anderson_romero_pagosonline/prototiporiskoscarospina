package prototipo.banco.web.wicket;

/**
 * La pagina base del cliente.
 * 
 * @author oscar.ospina
 *
 */
public class PaginaCliente extends PaginaBase {
	/**
	 * el numero serial utilizado para serializar la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor.
	 * 
	 * @param id component id.
	 */
	public PaginaCliente() {
	}
}
