package prototipo.banco.integracion.clienteMAF;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

/**
 * Clase que consume los servicios web de MAF
 * 
 * @author oscar.ospina
 *
 */
public class ClienteMAF {

	public static void main(String[] args) throws Exception {
		SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
		SOAPConnection soapConnection = soapConnectionFactory.createConnection();
		String url = "http://qa.maf.pagosonline.net/ws/ws/maf?wsdl";

		// preValidar
		SOAPMessage soapRequest = soapConnection.call(preValidar(), url);
		System.out.println();
		System.out.println("----------Response preValidar----------");
		soapRequest.writeTo(System.out);
		System.out.println();

		// PostValidar
		soapRequest = soapConnection.call(postValidar(), url);
		System.out.println();
		System.out.println("----------Response postValidar----------");
		soapRequest.writeTo(System.out);
		System.out.println();

		// registrarAlerta
		soapRequest = soapConnection.call(registrarAlerta(), url);
		System.out.println();
		System.out.println("----------Response registrarAlerta----------");
		soapRequest.writeTo(System.out);
		System.out.println();

		// consultarEstado
		soapRequest = soapConnection.call(consultarEstado(), url);
		System.out.println();
		System.out.println("----------Response consultarEstado----------");
		soapRequest.writeTo(System.out);
		System.out.println();

		// consultarEstado
		soapRequest = soapConnection.call(actualizarEstado(), url);
		System.out.println();
		System.out.println("----------Response actualizarEstado----------");
		soapRequest.writeTo(System.out);
		System.out.println();

		// consultarEstado
		soapRequest = soapConnection.call(consultarEstado(), url);
		System.out.println();
		System.out.println("----------Response consultarEstado----------");
		soapRequest.writeTo(System.out);
		System.out.println();
	}

	/**
	 * Consume el servicio prevalidar del MAF
	 * 
	 * @return El mensaje que se le va a enviar al MAF
	 * @throws Exception
	 */
	public static SOAPMessage preValidar() throws Exception {
		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage();
		SOAPPart soapPart = soapMessage.getSOAPPart();
		SOAPEnvelope soapEnvelope = soapPart.getEnvelope();
		soapEnvelope.addNamespaceDeclaration("ws", "http://ws.maf.pagosonline.com/");
		SOAPBody soapBody = soapEnvelope.getBody();
		SOAPElement soapElement = soapBody.addChildElement("preValidar", "ws");
		SOAPElement element1 = soapElement.addChildElement("arg0");
		element1.addTextNode("7");
		SOAPElement element2 = soapElement.addChildElement("arg1");
		element2.addTextNode("test.tech@pagosonline.com");
		SOAPElement element3 = soapElement.addChildElement("arg2");
		element3.addTextNode("123456");
		SOAPElement element4 = soapElement.addChildElement("arg3");
		element4.addTextNode("<![CDATA[<validar-pago><transaccion-id>123456789</transaccion-id><fecha-creacion>2011-09-10T09:00:00</fecha-creacion><prueba>fasle</prueba><pais>CO</pais><origen>1100</origen><usuario-vendedor><id>7</id></usuario-vendedor><usuario-comprador><huella><direccion-ip>127.127.127.127</direccion-ip><firma-dispositivo>asd89ahs98hf9a8</firma-dispositivo><user-agent>Mozilla/5.0 (Windows; U; Windows NT 6.1; enUS)</user-agent></huella></usuario-comprador><descripcion>XBOX 360</descripcion><valor>1000.00</valor><informacion-pago><tipo-medio-pago>1</tipo-medio-pago><medio-pago>10</medio-pago><moneda>COP</moneda></informacion-pago></validar-pago>]]>");
		System.out.println("----------preValidar------------");
		soapMessage.saveChanges();
		soapMessage.writeTo(System.out);
		return soapMessage;
	}

	/**
	 * Consume el servicio postvalidar del MAF
	 * 
	 * @return El mensaje que se le va a enviar al MAF
	 * @throws Exception
	 */
	public static SOAPMessage postValidar() throws Exception {
		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage();
		SOAPPart soapPart = soapMessage.getSOAPPart();
		SOAPEnvelope soapEnvelope = soapPart.getEnvelope();
		soapEnvelope.addNamespaceDeclaration("ws", "http://ws.maf.pagosonline.com/");
		SOAPBody soapBody = soapEnvelope.getBody();
		SOAPElement soapElement = soapBody.addChildElement("postValidar", "ws");
		SOAPElement element1 = soapElement.addChildElement("arg0");
		element1.addTextNode("7");
		SOAPElement element2 = soapElement.addChildElement("arg1");
		element2.addTextNode("test.tech@pagosonline.com");
		SOAPElement element3 = soapElement.addChildElement("arg2");
		element3.addTextNode("123456");
		soapMessage.saveChanges();
		SOAPElement element4 = soapElement.addChildElement("arg3");
		element4.addTextNode("123456789");
		// System.out.println("----------postValidar------------");
		// soapMessage.writeTo(System.out);
		return soapMessage;
	}

	/**
	 * Consume el servicio registrar alerta del MAF
	 * 
	 * @return El mensaje que se le va a enviar al MAF
	 * @throws Exception
	 */
	public static SOAPMessage registrarAlerta() throws Exception {
		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage();
		SOAPPart soapPart = soapMessage.getSOAPPart();
		SOAPEnvelope soapEnvelope = soapPart.getEnvelope();
		soapEnvelope.addNamespaceDeclaration("ws", "http://ws.maf.pagosonline.com/");
		SOAPBody soapBody = soapEnvelope.getBody();
		SOAPElement soapElement = soapBody.addChildElement("registrarAlerta", "ws");
		SOAPElement element1 = soapElement.addChildElement("arg0");
		element1.addTextNode("7");
		SOAPElement element2 = soapElement.addChildElement("arg1");
		element2.addTextNode("test.tech@pagosonline.com");
		SOAPElement element3 = soapElement.addChildElement("arg2");
		element3.addTextNode("123456");
		soapMessage.saveChanges();
		SOAPElement element4 = soapElement.addChildElement("arg3");
		element4.addTextNode("<alerta><tipo-alerta>1</tipo-alerta><prioridad>1</prioridad><descripcion>Descripción</descripcion><originador>Sistema de pagos</originador><titulo>Alerta</titulo></alerta>");
		System.out.println("----------registrarAlerta------------");
		soapMessage.writeTo(System.out);
		return soapMessage;
	}

	/**
	 * Consume el servicio consulta estardo del MAF
	 * 
	 * @return El mensaje que se le va a enviar al MAF
	 * @throws Exception
	 */
	public static SOAPMessage consultarEstado() throws Exception {
		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage();
		SOAPPart soapPart = soapMessage.getSOAPPart();
		SOAPEnvelope soapEnvelope = soapPart.getEnvelope();
		soapEnvelope.addNamespaceDeclaration("ws", "http://ws.maf.pagosonline.com/");
		SOAPBody soapBody = soapEnvelope.getBody();
		SOAPElement soapElement = soapBody.addChildElement("consultarEstado", "ws");
		SOAPElement element1 = soapElement.addChildElement("arg0");
		element1.addTextNode("7");
		SOAPElement element2 = soapElement.addChildElement("arg1");
		element2.addTextNode("test.tech@pagosonline.com");
		SOAPElement element3 = soapElement.addChildElement("arg2");
		element3.addTextNode("123456");
		soapMessage.saveChanges();
		SOAPElement element4 = soapElement.addChildElement("arg3");
		element4.addTextNode("123456789");
		// System.out.println("----------consultarEstado------------");
		// soapMessage.writeTo(System.out);
		return soapMessage;
	}

	/**
	 * Consume el servicio actualizar estado del MAF
	 * 
	 * @return El mensaje que se le va a enviar al MAF
	 * @throws Exception
	 */
	public static SOAPMessage actualizarEstado() throws Exception {
		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage();
		SOAPPart soapPart = soapMessage.getSOAPPart();
		SOAPEnvelope soapEnvelope = soapPart.getEnvelope();
		soapEnvelope.addNamespaceDeclaration("ws", "http://ws.maf.pagosonline.com/");
		SOAPBody soapBody = soapEnvelope.getBody();
		SOAPElement soapElement = soapBody.addChildElement("actualizarEstado", "ws");
		SOAPElement element1 = soapElement.addChildElement("arg0");
		element1.addTextNode("7");
		SOAPElement element2 = soapElement.addChildElement("arg1");
		element2.addTextNode("test.tech@pagosonline.com");
		SOAPElement element3 = soapElement.addChildElement("arg2");
		element3.addTextNode("123456");
		soapMessage.saveChanges();
		SOAPElement element4 = soapElement.addChildElement("arg3");
		element4.addTextNode("123456789");
		SOAPElement element5 = soapElement.addChildElement("arg4");
		element5.addTextNode("6");
		// System.out.println("----------consultarEstado------------");
		// soapMessage.writeTo(System.out);
		return soapMessage;
	}
}