package prototipo.banco.web.wicket;

/**
 * Representa la pagina inicial de la aplicacion web.
 */
public class PaginaInicio extends PaginaBase {

	/**
	 * el numero serial utilizado para serializar la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor de la pagina.
	 */
	public PaginaInicio() {
	}
}
