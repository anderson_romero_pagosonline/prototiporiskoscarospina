package prototipo.banco.comun.logica;

/**
 * Los tipos de movimiento validos
 * 
 * @author oscar.ospina
 *
 */
public enum TipoMovimiento {
	debito, credito
}
