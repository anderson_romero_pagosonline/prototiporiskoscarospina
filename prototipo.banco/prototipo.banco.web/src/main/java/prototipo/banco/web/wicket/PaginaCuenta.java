package prototipo.banco.web.wicket;

/**
 * Pagina basica de la cuenta.
 * 
 * @author oscar.ospina
 *
 */
public class PaginaCuenta extends PaginaBase {
	/**
	 * el numero serial utilizado para serializar la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Contructor de la pagina.
	 */
	public PaginaCuenta() {
	}
}
