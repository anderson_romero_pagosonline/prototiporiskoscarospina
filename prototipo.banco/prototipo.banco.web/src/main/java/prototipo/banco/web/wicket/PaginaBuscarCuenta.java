package prototipo.banco.web.wicket;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.markup.repeater.data.ListDataProvider;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;

import prototipo.banco.comun.logica.Cuenta;
import prototipo.banco.comun.logica.Movimiento;

/**
 * Clase que representa la pagina donde se busca una Cuenta.
 * 
 * @author oscar.ospina
 *
 */
public class PaginaBuscarCuenta extends PaginaBase {

	/**
	 * El numero serial utilizado para serializar la clase.
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Tabla de movimientos.
	 */
	private DataView<Movimiento> movimientosTable;
	/**
	 * Lista desplegable de las cuentas.
	 */
	private DropDownChoice<Integer> cuentasDrop;
	/**
	 * El campo que es el numero de identificacion del cliente.
	 */
	private Label clienteId;
	/**
	 * El campo que es el saldo de la cuenta.
	 */
	private Label saldo;
	/**
	 * El numero de identificación del cliente.
	 */
	private String clienteIdCuenta;
	/**
	 * El saldo de la cuenta.
	 */
	private BigDecimal saldoCuenta;
	/**
	 * lista con el numero de las cuentas.
	 */
	private List<Integer> numeroCuentas;
	/**
	 * lista de movimientos.
	 */
	private List<Movimiento> movimientos;
	/**
	 * Los datos que se van a agregar a la tabla.
	 */
	private ListDataProvider<Movimiento> listDataProvider;
	/**
	 * El Form basico de la pagina.
	 */
	private Form<String> form;

	/**
	 * Constructor de la pagina.
	 */
	public PaginaBuscarCuenta() {
		inicializarElementos();
		agregarElementos();
		hacerCamposObligatorios();
	}

	/**
	 * Vuelve el campo cuentasDrop requerido.
	 */
	public void hacerCamposObligatorios() {
		cuentasDrop.setRequired(true);
	}

	/**
	 * Instancia todos los elementos de la pagina Web.
	 */
	public void inicializarElementos() {
		movimientos = new ArrayList<Movimiento>();
		numeroCuentas = new ArrayList<Integer>();

		Cuenta[] cuentas = getGestor().buscarCuentas();
		agregarCuentas(cuentas);

		clienteId = new Label("cliente", new PropertyModel<String>(this, "clienteIdCuenta"));
		saldo = new Label("saldo", new PropertyModel<String>(this, "saldoCuenta"));
		form = new Form<String>("form");
		listDataProvider = new ListDataProvider<Movimiento>(movimientos);
		cuentasDrop = new DropDownChoice<Integer>("cuentas", new Model<Integer>(), numeroCuentas) {
			private static final long serialVersionUID = 1L;

			@Override
			protected void onSelectionChanged(final Integer newSelection) {
				actualizarDatos(newSelection);
			}

			@Override
			protected boolean wantOnSelectionChangedNotifications() {
				return true;
			}
		};
		movimientosTable = new DataView<Movimiento>("rows", listDataProvider) {
			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(Item<Movimiento> item) {
				Movimiento movimiento = item.getModelObject();
				RepeatingView repeatingView = new RepeatingView("dataRow");

				repeatingView.add(new Label(repeatingView.newChildId(), movimiento.getNumero()));
				repeatingView.add(new Label(repeatingView.newChildId(), movimiento.getValor()));
				repeatingView.add(new Label(repeatingView.newChildId(), movimiento.getTipo()));
				item.add(repeatingView);
			}
		};
	}

	/**
	 * Agrega los diferentes elementos a la pagina.
	 */
	public void agregarElementos() {
		form.add(movimientosTable);
		form.add(clienteId);
		form.add(saldo);
		form.add(cuentasDrop);
		form.add(new Button("eliminar") {
			private static final long serialVersionUID = 1L;

			@Override
			public void onSubmit() {
				eliminarCuenta(cuentasDrop.getModelObject());
				setResponsePage(PaginaInicio.class);
			}
		});
		form.add(new Link<String>("cancelar") {
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				setResponsePage(PaginaInicio.class);
			}
		});
		add(form);
		add(new FeedbackPanel("feedback"));
	}

	/**
	 * Actualiza los campos cuando se selecciona una cuenta.
	 * 
	 * @param seleccion la cuenta seleccionada.
	 */
	public void actualizarDatos(Integer selection) {
		Movimiento[] cuentas = getGestor().buscarMovimientoCuentas(selection);
		agregarMovimientos(cuentas);

		Cuenta cuenta = getGestor().buscarCuenta(selection);
		setSaldoCuenta(cuenta.getSaldo());
		setClienteIdCuenta(cuenta.getCliente().getNumeroDeIdentificacion());
	}

	/**
	 * Agrega los numero de las cuentas de la lista de cuentas.
	 * 
	 * @param cuentas la lista de cuentas, de los cuales se obtiene los numeros.
	 */
	public void agregarCuentas(Cuenta[] cuentas) {
		for (int i = 0; i < cuentas.length; i++) {
			numeroCuentas.add(cuentas[i].getNumero());
		}
	}

	/**
	 * agrega los movimientos a la lista de movimientos.
	 * 
	 * @param movimientos el vector de arreglos, el cual tiene los moviminentos
	 *        a agregar.
	 */
	public void agregarMovimientos(Movimiento[] movimientos) {
		this.movimientos.clear();
		this.movimientos.addAll(Arrays.asList(movimientos));
	}

	/**
	 * Elimina una cuenta dado su numero.
	 * 
	 * @param numero el numero con el cual se identifica la cuenta.
	 */
	public void eliminarCuenta(Integer numero) {
		getGestor().eliminarCuenta(numero);
	}

	public String getClienteIdCuenta() {
		return clienteIdCuenta;
	}

	public void setClienteIdCuenta(String clienteIdCuenta) {
		this.clienteIdCuenta = clienteIdCuenta;
	}

	public BigDecimal getSaldoCuenta() {
		return saldoCuenta;
	}

	public void setSaldoCuenta(BigDecimal saldoCuenta) {
		this.saldoCuenta = saldoCuenta;
	}
}
