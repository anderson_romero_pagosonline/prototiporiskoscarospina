package prototipo.banco.web.wicket;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.Model;

import prototipo.banco.comun.logica.Cliente;
import prototipo.banco.comun.logica.Cuenta;
import prototipo.banco.comun.logica.Movimiento;
import prototipo.banco.comun.logica.TipoMovimiento;

/**
 * Representa la pagina donde se crea un movimiento nuevo.
 * 
 * @author oscar.ospina
 *
 */
public class PaginaMovimiento extends PaginaBase {
	/**
	 * Lista desplegable de los numeros de identificación de los clientes.
	 */
	private DropDownChoice<String> numerosIdentificacion;
	/**
	 * Lista desplegable de los numeros de las cuentas.
	 */
	private DropDownChoice<Integer> cuenta;
	/**
	 * Lista desplegable de los tipos de movimientos.
	 */
	private DropDownChoice<String> tipoMovimiento;
	/**
	 * Es el campo de texto del numero del movimiento.
	 */
	private TextField<Integer> numero;
	/**
	 * Es el campo de texto del valor del movimiento.
	 */
	private TextField<BigDecimal> valor;
	/**
	 * la lista de numeros de identificación.
	 */
	private List<String> cedulas;
	/**
	 * lista de os numeros de las cuentas.
	 */
	private List<Integer> numeroCuentas;
	/**
	 * El Form basico de la pagina
	 */
	private Form<String> form;
	/**
	 * el numero serial utilizado para serializar la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor de la pagina
	 */
	public PaginaMovimiento() {
		inicializarElementos();
		agregarElementos();
		hacerCamposObligatorios();
	}

	/**
	 * Vuelve los campos numeroIdentificacion, nombre, direccion y telefono
	 * requeridos.
	 */
	public void hacerCamposObligatorios() {
		numerosIdentificacion.setRequired(true);
		cuenta.setRequired(true);
		tipoMovimiento.setRequired(true);
		numero.setRequired(true);
		valor.setRequired(true);
	}

	/**
	 * Instancia todos los elementos de la pagina Web.
	 */
	public void inicializarElementos() {
		cedulas = new ArrayList<String>();
		numeroCuentas = new ArrayList<Integer>();
		List<String> tipos = new ArrayList<String>();
		tipos.add(TipoMovimiento.credito.toString());
		tipos.add(TipoMovimiento.debito.toString());

		Cliente[] clientes = getGestor().buscarClientes();
		getCedulasClientes(clientes);

		numerosIdentificacion = new DropDownChoice<String>("numeroIdentificacion",
				new Model<String>(""), cedulas) {
			private static final long serialVersionUID = 1L;

			@Override
			protected void onSelectionChanged(final String newSelection) {
				Cuenta[] cuentas = getGestor().buscarCuentasCliente(newSelection);
				numeroCuentas(cuentas);
			}

			@Override
			protected boolean wantOnSelectionChangedNotifications() {
				return true;
			}
		};
		cuenta = new DropDownChoice<Integer>("cuenta", new Model<Integer>(), numeroCuentas);
		tipoMovimiento = new DropDownChoice<String>("tipo", new Model<String>(), tipos);
		numero = new TextField<Integer>("numero", new Model<Integer>(), Integer.class);
		valor = new TextField<BigDecimal>("valor", new Model<BigDecimal>(), BigDecimal.class);
		form = new Form<String>("form");

		Cuenta[] cuentas = getGestor().buscarCuentasCliente("");
		numeroCuentas(cuentas);
	}

	/**
	 * Agrega los diferentes elementos a la pagina.
	 */
	public void agregarElementos() {
		form.add(numerosIdentificacion);
		form.add(numero);
		form.add(tipoMovimiento);
		form.add(cuenta);
		form.add(valor);
		form.add(new Button("crear") {
			private static final long serialVersionUID = 1L;

			@Override
			public void onSubmit() {
				crearMovimiento(numero.getModelObject(), tipoMovimiento.getModelObject(),
						new Date(), valor.getModelObject(), cuenta.getModelObject());
				setResponsePage(PaginaInicio.class);
			}
		});
		form.add(new Link<String>("cancelar") {
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				setResponsePage(PaginaInicio.class);
			}
		});
		add(form);
		add(new FeedbackPanel("feedback"));
	}

	/**
	 * Crea un movimiento nuevo.
	 * 
	 * @param numeroMovimiento el numero con el cual se identifica el movimiento.
	 * @param tipoMovimiento tipo de movimiento (debito o credito).
	 * @param fecha la fecha en la cual se se realiza el movimiento.
	 * @param valor por el cual se hace el movimiento.
	 * @param numeroCuenta el numero de la cuenta a la cual se le aplica el
	 *        movimiento.
	 */
	public void crearMovimiento(Integer numeroMovimiento, String tipoMovimiento, Date fecha,
			BigDecimal valor, Integer numeroCuenta) {
		Cuenta cuenta = getGestor().buscarCuenta(numeroCuenta);
		Movimiento m = new Movimiento(numeroMovimiento, tipoMovimiento, fecha, valor, cuenta);
		getGestor().creaMovimiento(m);
	}

	/**
	 * Agrega los numero de las cuentas de la lista de cuentas.
	 * 
	 * @param cuentas la lista de cuentas, de los cuales se obtiene los numeros.
	 */
	public void numeroCuentas(Cuenta[] cuentas) {
		numeroCuentas.clear();
		for (int i = 0; i < cuentas.length; i++) {
			numeroCuentas.add(cuentas[i].getNumero());
		}
	}

	/**
	 * Agrega los numero de identificación de la lista de clientes.
	 * 
	 * @param clientes la lista de clientes, de los cuales se obtiene los
	 *        numeros de identificación.
	 */
	public void getCedulasClientes(Cliente[] clientes) {
		for (int i = 0; i < clientes.length; i++) {
			cedulas.add(clientes[i].getNumeroDeIdentificacion());
		}
	}
}
