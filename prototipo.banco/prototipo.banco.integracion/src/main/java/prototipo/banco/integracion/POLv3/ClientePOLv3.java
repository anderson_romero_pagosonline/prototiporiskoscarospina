package prototipo.banco.integracion.POLv3;

import java.io.IOException;

import javax.xml.soap.SOAPHeader;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

/**
 * Clase que consume los servicios de POLv3
 * 
 * @author oscar.ospina
 *
 */
public class ClientePOLv3 {

	public static void main(String[] args) throws Exception {
		SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
		SOAPConnection soapConnection = soapConnectionFactory.createConnection();
		String url = "http://qa.gateway.pagosonline.net/ws/WebServicesClientesUT?wsdl";
		// preValidar
		SOAPMessage soapRequest = soapConnection.call(solicitarAutorizacion(), url);
		System.out.println();
		System.out.println("----------Response solicitarAutorizacion----------");
		soapRequest.writeTo(System.out);
		System.out.println();
	}

	/**
	 * Consume el servicio solicitar autorización de POLv3
	 * 
	 * @return El mensaje que se le va a enviar a POLv3
	 * @throws Exception
	 */
	public static SOAPMessage solicitarAutorizacion() throws SOAPException, IOException {
		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage();
		SOAPPart soapPart = soapMessage.getSOAPPart();
		SOAPEnvelope soapEnvelope = soapPart.getEnvelope();
		soapEnvelope.addNamespaceDeclaration("ser",
				"http://server.webservices.web.v2.pagosonline.net");
		soapEnvelope
				.addNamespaceDeclaration("wsse",
						"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
		SOAPHeader header = soapEnvelope.getHeader();
		SOAPElement elementh1 = header.addChildElement("Security", "wsse");
		SOAPElement elementh2 = elementh1.addChildElement("UsernameToken", "wsse");
		SOAPElement elementh3 = elementh2.addChildElement("Username", "wsse");
		elementh3.addTextNode("7");
		SOAPElement elementh4 = elementh2.addChildElement("Password", "wsse");
		elementh4.addTextNode("minutos123");
		SOAPBody soapBody = soapEnvelope.getBody();
		SOAPElement soapElement = soapBody.addChildElement("solicitarAutorizacion", "ser");
		SOAPElement soapElement1 = soapElement.addChildElement("solicitarAutorizacion");
		SOAPElement element1 = soapElement1.addChildElement("baseDevolucionIva");
		element1.addTextNode("0");
		SOAPElement element2 = soapElement1.addChildElement("ciudadCorrespondencia");
		element2.addTextNode("Bogota");
		SOAPElement element3 = soapElement1.addChildElement("ciudadEnvio");
		element3.addTextNode("Bogota");
		SOAPElement element4 = soapElement1.addChildElement("codigoSeguridad");
		element4.addTextNode("768");
		SOAPElement element5 = soapElement1.addChildElement("cuentaId");
		element5.addTextNode("10062");
		SOAPElement element6 = soapElement1.addChildElement("descripcion");
		element6.addTextNode("DESCRIPCION");
		SOAPElement element7 = soapElement1.addChildElement("direccionCorrespondencia");
		element7.addTextNode("avenida siempre viva 123");
		SOAPElement element8 = soapElement1.addChildElement("direccionEnvio");
		element8.addTextNode("avenida siempre viva 123");
		SOAPElement element9 = soapElement1.addChildElement("documentoIdentificacion");
		element9.addTextNode("123456789");
		SOAPElement element10 = soapElement1.addChildElement("emailComprador");
		element10.addTextNode("test.tech.PRUEBA@gmail.com");
		SOAPElement element11 = soapElement1.addChildElement("fechaExpiracion");
		element11.addTextNode("2015/04");
		SOAPElement element12 = soapElement1.addChildElement("franquicia");
		element12.addTextNode("VISA");
		SOAPElement element13 = soapElement1.addChildElement("ipComprador");
		element13.addTextNode("190.144.2.42");
		SOAPElement element14 = soapElement1.addChildElement("isoMoneda4217");
		element14.addTextNode("COP");
		SOAPElement element15 = soapElement1.addChildElement("iva");
		element15.addTextNode("0");
		SOAPElement element16 = soapElement1.addChildElement("nombreComprador");
		element16.addTextNode("Prueba");
		SOAPElement element17 = soapElement1.addChildElement("nombreTarjetaHabiente");
		element17.addTextNode("Prueba");
		SOAPElement element18 = soapElement1.addChildElement("numero");
		element18.addTextNode("4559864234326789");
		SOAPElement element19 = soapElement1.addChildElement("numeroCuotas");
		element19.addTextNode("1");
		SOAPElement element20 = soapElement1.addChildElement("paisCorrespondencia");
		element20.addTextNode("CO");
		SOAPElement element21 = soapElement1.addChildElement("paisEnvio");
		element21.addTextNode("CO");
		SOAPElement element22 = soapElement1.addChildElement("referencia");
		element22.addTextNode("WS POL JIO 2015-02-06 000029");
		SOAPElement element23 = soapElement1.addChildElement("telefono");
		element23.addTextNode("13465456");
		SOAPElement element24 = soapElement1.addChildElement("userAgent");
		element24.addTextNode("12345613");
		SOAPElement element25 = soapElement1.addChildElement("validarModuloAntiFraude");
		element25.addTextNode("1");
		SOAPElement element26 = soapElement1.addChildElement("valor");
		element26.addTextNode("10");
		System.out.println("----------preValidar------------");
		soapMessage.writeTo(System.out);
		soapMessage.saveChanges();
		return soapMessage;
	}

	/**
	 * Consume el servicio version autorización de POLv3
	 * 
	 * @return El mensaje que se le va a enviar a POLv3
	 * @throws Exception
	 */
	public static SOAPMessage version() throws SOAPException, IOException {
		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage();
		SOAPPart soapPart = soapMessage.getSOAPPart();
		SOAPEnvelope soapEnvelope = soapPart.getEnvelope();
		soapEnvelope.addNamespaceDeclaration("ser",
				"http://server.webservices.web.v2.pagosonline.net");
		soapEnvelope
				.addNamespaceDeclaration("wsse",
						"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
		SOAPHeader header = soapEnvelope.getHeader();
		SOAPElement element1 = header.addChildElement("Security", "wsse");
		SOAPElement element2 = element1.addChildElement("UsernameToken", "wsse");
		SOAPElement element3 = element2.addChildElement("Username", "wsse");
		element3.addTextNode("7");
		SOAPElement element4 = element2.addChildElement("Password", "wsse");
		element4.addTextNode("minutos123");
		SOAPBody soapBody = soapEnvelope.getBody();
		SOAPElement soapElement = soapBody.addChildElement("getVersion", "ser");

		System.out.println("----------solicitarAutorizacion------------");
		soapMessage.writeTo(System.out);
		return soapMessage;
	}
}
