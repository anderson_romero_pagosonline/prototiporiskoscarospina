package prototipo.banco.web.wicket;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.markup.repeater.data.ListDataProvider;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;

import prototipo.banco.comun.logica.Cliente;
import prototipo.banco.comun.logica.Cuenta;

/**
 * Clase que representa la pagina donde se busca un Cliente.
 * 
 * @author oscar.ospina
 *
 */
public class PaginaBuscarCliente extends PaginaBase {

	/**
	 * El numero serial utilizado para serializar la clase.
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Es la lista desplegable con los numeros de identificacion de los clientes.
	 */
	private DropDownChoice<String> numeroIdentificacion;
	/**
	 * Tabla de cuentas.
	 */
	private DataView<Cuenta> cuentasTable;
	/**
	 * El campo de nombre del cliente.
	 */
	private Label nombre;
	/**
	 * El nombre del cliente.
	 */
	private String nombreCliente;
	/**
	 * El campo del telefono del cliente.
	 */
	private Label telefono;
	/**
	 * El telefono del cliente.
	 */
	private String telefonoCliente;
	/**
	 * El campo de la dirección del cliente.
	 */
	private Label direccion;
	/**
	 * la dirección del cliente.
	 */
	private String direccionCliente;
	/**
	 * Las lista de las cedulas de los cleintes.
	 */
	private List<String> cedulas;
	/**
	 * lista de las cuentas de un cliente.
	 */
	private List<Cuenta> cuentas;
	/**
	 * Los datos que se van a agregar a la tabla.
	 */
	private ListDataProvider<Cuenta> listDataProvider;
	/**
	 * El Form basico de la pagina.
	 */
	private Form<String> form;

	/**
	 * Constructor de la pagina.
	 */
	public PaginaBuscarCliente() {
		inicializarElementos();
		agregarElementos();
		hacerCamposObligatorios();
	}

	/**
	 * Vuelve el campo numeroIdentificacion requerido.
	 */
	public void hacerCamposObligatorios() {
		numeroIdentificacion.setRequired(true);
	}

	/**
	 * Instancia todos los elementos de la pagina Web.
	 */
	public void inicializarElementos() {
		cedulas = new ArrayList<String>();
		cuentas = new ArrayList<Cuenta>();

		Cliente[] clientes = getGestor().buscarClientes();
		cedulasClientes(clientes);
		telefono = new Label("telefono", new PropertyModel<String>(this, "telefonoCliente"));
		nombre = new Label("nombre", new PropertyModel<String>(this, "nombreCliente"));
		direccion = new Label("direccion", new PropertyModel<String>(this, "direccionCliente"));
		form = new Form<String>("form");
		listDataProvider = new ListDataProvider<Cuenta>(cuentas);
		numeroIdentificacion = new DropDownChoice<String>("numeroIdentificacion",
				new Model<String>(""), cedulas) {
			private static final long serialVersionUID = 1L;

			@Override
			protected void onSelectionChanged(final String newSelection) {
				actualizarDatos(newSelection);
			}

			@Override
			protected boolean wantOnSelectionChangedNotifications() {
				return true;
			}
		};
		cuentasTable = new DataView<Cuenta>("rows", listDataProvider) {
			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(Item<Cuenta> item) {
				Cuenta movimiento = item.getModelObject();
				RepeatingView repeatingView = new RepeatingView("dataRow");

				repeatingView.add(new Label(repeatingView.newChildId(), movimiento.getNumero()));
				repeatingView.add(new Label(repeatingView.newChildId(), movimiento.getSaldo()));
				item.add(repeatingView);
			}
		};
	}

	/**
	 * Agrega los diferentes elementos a la pagina.
	 */
	public void agregarElementos() {
		form.add(numeroIdentificacion);
		form.add(nombre);
		form.add(telefono);
		form.add(direccion);
		form.add(cuentasTable);
		form.add(new Button("eliminar") {
			private static final long serialVersionUID = 1L;

			@Override
			public void onSubmit() {
				eliminarCliente(numeroIdentificacion.getModelObject());
				setResponsePage(PaginaInicio.class);
			}
		});
		form.add(new Link<String>("cancelar") {
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				setResponsePage(PaginaInicio.class);
			}
		});
		add(form);
		add(new FeedbackPanel("feedback"));
	}

	/**
	 * Agrega los numero de identificación a la lista cedulas dado una lista de
	 * clientes.
	 * 
	 * @param clientes la lista de clientes, de los qcuales se obtiene los
	 *        numero de identificación.
	 */
	public void cedulasClientes(Cliente[] clientes) {
		cedulas.clear();
		for (int i = 0; i < clientes.length; i++) {
			cedulas.add(clientes[i].getNumeroDeIdentificacion());
		}
	}

	/**
	 * Elimina un ciente.
	 * 
	 * @param numeroId el numero de identificación del cliente a eliminar.
	 */
	public void eliminarCliente(String numeroId) {
		getGestor().eliminarCliente(numeroId);
	}

	/**
	 * Agrega las cuentas a la lista de cuentas de un vector de cuentas.
	 * 
	 * @param cuentas es el vector que tiene las cuentas.
	 */
	public void agregarCuentas(Cuenta[] cuentas) {
		this.cuentas.clear();
		this.cuentas.addAll(Arrays.asList(cuentas));
	}

	/**
	 * Actualiza los campos cuando se selecciona un cliente.
	 * 
	 * @param seleccion el cliente seleccionado.
	 */
	public void actualizarDatos(String seleccion) {
		agregarCuentas(getGestor().buscarCuentasCliente(seleccion));
		Cliente cliente = getGestor().buscarCliente(seleccion);
		setNombreCliente(cliente.getNombre());
		setTelefonoCliente(cliente.getTelefono());
		setDireccionCliente(cliente.getDireccion());
	}

	public String getNombreCliente() {
		return nombreCliente;
	}

	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

	public String getTelefonoCliente() {
		return telefonoCliente;
	}

	public void setTelefonoCliente(String telefonoCliente) {
		this.telefonoCliente = telefonoCliente;
	}

	public String getDireccionCliente() {
		return direccionCliente;
	}

	public void setDireccionCliente(String direccionCliente) {
		this.direccionCliente = direccionCliente;
	}
}