package prototipo.banco.web.wicket;

import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.Model;

import prototipo.banco.comun.logica.Cliente;

/**
 * Representa la pagina en la cual se agrega un cliente.
 * 
 * @author oscar.ospina
 *
 */
public class PaginaClienteCrear extends PaginaBase {
	/**
	 * El numero serial utilizado para serializar la clase.
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * El campo de texto del numero de identificación del cliente.
	 */
	private TextField<String> numeroIdentificacion;
	/**
	 * El campo de texto de nombre del cliente.
	 */
	private TextField<String> nombre;
	/**
	 * El campo de texto de direccion del cliente.
	 */
	private TextField<String> direccion;
	/**
	 * El campo de texto de telefono del cliente.
	 */
	private TextField<String> telefono;
	/**
	 * El Form basico de la pagina.
	 */
	private Form<String> form;

	/**
	 * Constructor de la pagina.
	 */
	public PaginaClienteCrear() {
		inicializarElementos();
		agregarElementos();
		hacerCamposObligatorios();
	}

	/**
	 * Vuelve los campos numeroIdentificacion, nombre, direccion y telefono
	 * requeridos.
	 */
	public void hacerCamposObligatorios() {
		numeroIdentificacion.setRequired(true);
		nombre.setRequired(true);
		direccion.setRequired(true);
		telefono.setRequired(true);
	}

	/**
	 * Agrega los diferentes elementos a la pagina.
	 */
	public void agregarElementos() {
		form.add(numeroIdentificacion);
		form.add(nombre);
		form.add(direccion);
		form.add(telefono);
		form.add(new Button("crear") {
			private static final long serialVersionUID = 1L;

			@Override
			public void onSubmit() {
				getGestor().crearCliente(
						new Cliente(numeroIdentificacion.getModelObject(), nombre.getModelObject(),
								direccion.getModelObject(), telefono.getModelObject()));
				setResponsePage(PaginaInicio.class);
			}
		});
		form.add(new Link<String>("cancelar") {
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				setResponsePage(PaginaInicio.class);
			}
		});
		add(form);
		add(new FeedbackPanel("feedback"));
	}

	/**
	 * Instancia todos los elementos de la pagina Web.
	 */
	public void inicializarElementos() {
		numeroIdentificacion = new TextField<String>("numeroIdentificacion", new Model<String>(""));
		nombre = new TextField<String>("nombre", new Model<String>(""));
		direccion = new TextField<String>("direccion", new Model<String>(""));
		telefono = new TextField<String>("telefono", new Model<String>(""));
		form = new Form<String>("form");
	}
}
