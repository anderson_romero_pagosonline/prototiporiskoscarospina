package prototipo.banco.web.wicket;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.wicket.extensions.yui.calendar.DateTimeField;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.markup.repeater.data.ListDataProvider;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;

import prototipo.banco.comun.logica.Cliente;
import prototipo.banco.comun.logica.Cuenta;
import prototipo.banco.comun.logica.Movimiento;

/**
 * Representa la pagina de los reportes
 * 
 * @author oscar.ospina
 *
 */
public class PaginaReporte extends PaginaBase {

	/**
	 * el numero serial utilizado para serializar la clase.
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Lista desplegable de los numeros de identificación de los clientes.
	 */
	private DropDownChoice<String> numeroIdentificacion;
	/**
	 * Lista desplegable de los numeros de las cuentas.
	 */
	private DropDownChoice<Integer> cuentasDrop;
	/**
	 * la lista de numeros de identificación.
	 */
	private List<String> cedulas;
	/**
	 * Campo de la fecha de inicio del reporte.
	 */
	private Date fechaInicio;
	/**
	 * Campo de la fecha final del reporte.
	 */
	private Date fechaFin;
	/**
	 * Tabla de los movimiento.
	 */
	private DataView<Movimiento> dataView;
	/**
	 * Fecha de Inicio del reporte.
	 */
	private DateTimeField fechaInicial;
	/**
	 * Fecha final del reporte.
	 */
	private DateTimeField fechaFinal;
	/**
	 * Lista de los numeros de las cuentas.
	 */
	private List<Integer> numeroCuentas;
	/**
	 * El Form basico de la pagina.
	 */
	private Form<String> form;
	/**
	 * Datos que con los cuales se van a poblar la tabla.
	 */
	private ListDataProvider<Movimiento> listDataProvider;
	/**
	 * lista con los movimientos.
	 */
	private List<Movimiento> movimientos;
	/**
	 * Campo del total de los debitos.
	 */
	private Label totalDebitos;
	/**
	 * Campo del total de los creditos.
	 */
	private Label totalCreditos;
	/**
	 * Total de los creditos.
	 */
	private BigDecimal credito;
	/**
	 * Total de lo debitos.
	 */
	private BigDecimal debito;

	/**
	 * constructor
	 */
	public PaginaReporte() {
		inicializarElementos();
		agregarElementos();
		hacerCamposObligatorios();
	}

	/**
	 * Instancia todos los elementos de la pagina Web.
	 */
	public void inicializarElementos() {
		setFechaInicio(new Date());
		setFechaFin(new Date());

		form = new Form<String>("form");
		PropertyModel<Date> modelInicial = new PropertyModel<Date>(this, "fechaInicio");
		PropertyModel<Date> modelFinal = new PropertyModel<Date>(this, "fechaFin");

		numeroCuentas = new ArrayList<Integer>();
		cedulas = new ArrayList<String>();

		Cliente[] clientes = getGestor().buscarClientes();
		cedulasClientes(clientes);

		totalCreditos = new Label("creditos", new PropertyModel<String>(this, "credito"));
		totalDebitos = new Label("debitos", new PropertyModel<String>(this, "debito"));
		fechaInicial = new DateTimeField("fechaInicial", modelInicial);
		fechaFinal = new DateTimeField("fechaFinal", modelFinal);
		cuentasDrop = new DropDownChoice<Integer>("cuentas", new Model<Integer>(), numeroCuentas);
		movimientos = new ArrayList<Movimiento>();
		listDataProvider = new ListDataProvider<Movimiento>(movimientos);
		numeroIdentificacion = new DropDownChoice<String>("numeroIdentificacion",
				new Model<String>(""), cedulas) {
			private static final long serialVersionUID = 1L;

			@Override
			protected void onSelectionChanged(final String newSelection) {
				actualizarDatos(newSelection);
			}

			@Override
			protected boolean wantOnSelectionChangedNotifications() {
				return true;
			}
		};
		dataView = new DataView<Movimiento>("rows", listDataProvider) {
			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(Item<Movimiento> item) {
				Movimiento movimiento = item.getModelObject();
				RepeatingView repeatingView = new RepeatingView("dataRow");

				repeatingView.add(new Label(repeatingView.newChildId(), movimiento.getNumero()));
				repeatingView.add(new Label(repeatingView.newChildId(), movimiento.getValor()));
				repeatingView.add(new Label(repeatingView.newChildId(), movimiento.getTipo()));
				item.add(repeatingView);
			}
		};
	}

	/**
	 * Agrega los diferentes elementos a la pagina.
	 */
	public void agregarElementos() {
		form.add(dataView);
		form.add(fechaInicial);
		form.add(fechaFinal);
		form.add(cuentasDrop);
		form.add(totalCreditos);
		form.add(totalDebitos);
		form.add(numeroIdentificacion);
		form.add(new Button("consultar") {
			private static final long serialVersionUID = 1L;

			@Override
			public void onSubmit() {
				generarReporte(cuentasDrop.getModelObject());
			}
		});
		form.add(new Link<String>("cancelar") {
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				setResponsePage(PaginaInicio.class);
			}
		});
		add(form);
		add(new FeedbackPanel("feedback"));
	}

	/**
	 * Actualiza los campos cuando se selecciona un cliente.
	 * 
	 * @param seleccion el cliente seleccionado.
	 */
	public void actualizarDatos(String selection) {
		numeroCuentas(getGestor().buscarCuentasCliente(selection));
	}

	/**
	 * Vuelve los campos numeroIdentificacion, nombre, direccion y telefono
	 * requeridos.
	 */
	public void hacerCamposObligatorios() {
		fechaInicial.setRequired(true);
		fechaFinal.setRequired(true);
		cuentasDrop.setRequired(true);
	}

	/**
	 * Genera el reporte de la cuenta.
	 * 
	 * @param numeroCuenta el numero con el cual se identifica la cuenta.
	 */
	public void generarReporte(Integer numeroCuenta) {
		movimientos.clear();
		movimientos.addAll(Arrays.asList(getGestor().buscarMovimientoCuentas(numeroCuenta,
				fechaInicio, fechaFin)));
		Cuenta cu = getGestor().buscarCuenta(numeroCuenta);
		setCredito(cu.buscarCreditosFecha(fechaInicio, fechaFin));
		setDebito(cu.buscarDebitosFecha(fechaInicio, fechaFin));
	}

	/**
	 * Agrega los numero de identificación a la lista cedulas dado una lista de
	 * clientes.
	 * 
	 * @param clientes la lista de clientes, de los qcuales se obtiene los
	 *        numero de identificación.
	 */
	public void cedulasClientes(Cliente[] clientes) {
		for (int i = 0; i < clientes.length; i++) {
			cedulas.add(clientes[i].getNumeroDeIdentificacion());
		}
	}

	/**
	 * Agrega los numero de las cuentas de la lista de cuentas.
	 * 
	 * @param cuentas la lista de cuentas, de los cuales se obtiene los numeros.
	 */
	public void numeroCuentas(Cuenta[] cuentas) {
		numeroCuentas.clear();
		for (int i = 0; i < cuentas.length; i++) {
			numeroCuentas.add(cuentas[i].getNumero());
		}
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public BigDecimal getCredito() {
		return credito;
	}

	public void setCredito(BigDecimal credito) {
		this.credito = credito;
	}

	public BigDecimal getDebito() {
		return debito;
	}

	public void setDebito(BigDecimal debito) {
		this.debito = debito;
	}
}