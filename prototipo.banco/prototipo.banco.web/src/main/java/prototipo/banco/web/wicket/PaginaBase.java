package prototipo.banco.web.wicket;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.spring.injection.annot.SpringBean;

import prototipo.banco.comun.gestor.IGestor;

/**
 * Representa la pagina base de la aplicación Web.
 * 
 * @author oscar.ospina
 *
 */
public class PaginaBase extends WebPage {

	/**
	 * el numero serial utilizado para serializar la clase.
	 */
	private static final long serialVersionUID = 1L;
	@SpringBean
	private IGestor gestor;

	/**
	 * Constructor.
	 */
	public PaginaBase() {
	}

	public IGestor getGestor() {
		return gestor;
	}

	public void setGestor(IGestor gestor) {
		this.gestor = gestor;
	}

}
