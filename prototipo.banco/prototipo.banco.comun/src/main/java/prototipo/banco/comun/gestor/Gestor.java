package prototipo.banco.comun.gestor;

import java.util.Date;

import org.openspaces.core.GigaSpace;
import org.openspaces.core.GigaSpaceConfigurer;
import org.openspaces.core.space.UrlSpaceConfigurer;
import org.springframework.stereotype.Service;

import prototipo.banco.comun.logica.Cliente;
import prototipo.banco.comun.logica.Cuenta;
import prototipo.banco.comun.logica.Movimiento;

import com.j_spaces.core.IJSpace;
import com.j_spaces.core.client.SQLQuery;

/**
 * Es la clase que se conecta con gigaspaces e implementa los metodos de la
 * interfaz IGestor
 * 
 * @author oscar.ospina
 *
 */
@Service
public class Gestor implements IGestor {

	/**
	 * la URL del espacio.
	 */
	private final String urlSpace = "jini://*/*/processorSpace";
	/**
	 * El espacio
	 */
	private GigaSpace gigaSpace = null;
	/**
	 * La unica instancia de esta clase
	 */
	private static Gestor gestor;

	/**
	 * El metodo que asegura que solo existe una instancia del gigaspaces y de
	 * esta clase
	 * 
	 * @return la unica instancia de esta clase
	 */
	public static Gestor getInstance() {
		if (gestor == null) {
			gestor = new Gestor();
		}
		return gestor;
	}

	private Gestor() {
		IJSpace space = new UrlSpaceConfigurer(urlSpace).space();
		gigaSpace = new GigaSpaceConfigurer(space).gigaSpace();
	}

	public void crearCliente(Cliente cliente) {
		gigaSpace.write(cliente);
	}

	public void crearCuenta(Cuenta cuenta) {
		gigaSpace.write(cuenta);
	}

	public Cliente[] buscarClientes() {
		SQLQuery<Cliente> cliQuery = new SQLQuery<Cliente>(Cliente.class, "");
		return gigaSpace.readMultiple(cliQuery);
	}

	public Cliente buscarCliente(String numeroIdCliente) {
		SQLQuery<Cliente> cliQuery = new SQLQuery<Cliente>(Cliente.class,
				"numeroDeIdentificacion=?");
		cliQuery.setParameter(1, numeroIdCliente);
		return gigaSpace.read(cliQuery);
	}

	public Cuenta buscarCuenta(Integer numeroCuenta) {
		SQLQuery<Cuenta> cuenQuery = new SQLQuery<Cuenta>(Cuenta.class, "numero=?");
		cuenQuery.setParameter(1, numeroCuenta);
		return gigaSpace.read(cuenQuery);
	}

	public void creaMovimiento(Movimiento movimiento) {
		gigaSpace.write(movimiento);
	}

	public Cuenta[] buscarCuentasCliente(String newSelection) {
		SQLQuery<Cuenta> cueQuery = new SQLQuery<Cuenta>(Cuenta.class,
				"cliente.numeroDeIdentificacion=?");
		cueQuery.setParameter(1, newSelection);
		return gigaSpace.readMultiple(cueQuery);
	}

	public void eliminarCliente(String numeroId) {
		SQLQuery<Cliente> cliQuery = new SQLQuery<Cliente>(Cliente.class,
				"numeroDeIdentificacion=?");
		cliQuery.setParameter(1, numeroId);
		eliminarCuentasCliente(numeroId);
		gigaSpace.clear(cliQuery);
	}

	public void eliminarCuentasCliente(String numeroId) {
		SQLQuery<Cuenta> cueQuery = new SQLQuery<Cuenta>(Cuenta.class,
				"cliente.numeroDeIdentificacion=?");
		cueQuery.setParameter(1, numeroId);
		Cuenta[] cuentas = gigaSpace.takeMultiple(cueQuery);
		for (int i = 0; i < cuentas.length; i++) {
			eliminarCuenta(cuentas[i].getNumero());
		}
	}

	public void eliminarCuenta(Integer numero) {
		SQLQuery<Cuenta> cueQuery = new SQLQuery<Cuenta>(Cuenta.class, "numero=?");
		cueQuery.setParameter(1, numero);
		eliminarMovimientosCuenta(numero);
		gigaSpace.clear(cueQuery);
	}

	public void eliminarMovimientosCuenta(Integer numero) {
		SQLQuery<Movimiento> movQuery = new SQLQuery<Movimiento>(Movimiento.class,
				"cuenta.numero=?");
		movQuery.setParameter(1, numero);
		Movimiento[] mov = gigaSpace.takeMultiple(movQuery);
		for (int i = 0; i < mov.length; i++) {
			eliminarMovimiento(mov[i]);
		}
	}

	public void eliminarMovimiento(Movimiento movimiento) {
		SQLQuery<Movimiento> movQuery = new SQLQuery<Movimiento>(Movimiento.class, "numero=?");
		movQuery.setParameter(1, movimiento.getNumero());
		gigaSpace.clear(movQuery);
	}

	public Cuenta[] buscarCuentas() {
		SQLQuery<Cuenta> cueQuery = new SQLQuery<Cuenta>(Cuenta.class, "");
		return gigaSpace.readMultiple(cueQuery);
	}

	public Movimiento[] buscarMovimientoCuentas(Integer numeroCuenta) {
		SQLQuery<Movimiento> cueQuery = new SQLQuery<Movimiento>(Movimiento.class,
				"cuenta.numero=?");
		cueQuery.setParameter(1, numeroCuenta);
		return gigaSpace.readMultiple(cueQuery);
	}

	public Movimiento[] buscarMovimientos() {
		SQLQuery<Movimiento> movQuery = new SQLQuery<Movimiento>(Movimiento.class, "c");
		return gigaSpace.readMultiple(movQuery);
	}

	public Movimiento[] buscarMovimientoCuentas(Integer numeroCuenta, Date fechaInicio,
			Date fechaFin) {
		SQLQuery<Movimiento> movQuery = new SQLQuery<Movimiento>(Movimiento.class,
				"cuenta.numero=? AND fecha >= ?  AND fecha <= ?");
		movQuery.setParameter(1, numeroCuenta);
		movQuery.setParameter(2, fechaInicio);
		movQuery.setParameter(3, fechaFin);
		return gigaSpace.readMultiple(movQuery);
	}
}
