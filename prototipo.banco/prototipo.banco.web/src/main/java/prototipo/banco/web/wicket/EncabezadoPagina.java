package prototipo.banco.web.wicket;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.panel.Panel;

/**
 * La clase que representa el encabezado donde esta el borde de la barra de
 * navegacion.
 * 
 * @author oscar.ospina
 *
 */
public class EncabezadoPagina extends Panel {

	/**
	 * el numero serial utilizado para serializar la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Contructor del encabezado.
	 * 
	 * @param id identificación del encabezado.
	 * @param exampleTitle titulo del encabezado.
	 * @param page Pagina Web donde esta el panel.
	 */
	public EncabezadoPagina(String id, String exampleTitle, WebPage page) {
		super(id);
		add(new Borde("navomaticBorder"));
	}
}
