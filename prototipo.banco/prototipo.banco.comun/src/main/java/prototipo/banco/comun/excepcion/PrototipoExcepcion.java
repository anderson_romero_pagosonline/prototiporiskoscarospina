package prototipo.banco.comun.excepcion;

/**
 * Clase que representa las exepciones de la capa logica.
 * 
 * @author Oscar Ospina
 *
 */
public class PrototipoExcepcion extends Exception {
	/**
	 * el numero serial utilizado para serializar la clase
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Crea una nueva exepcion a nivel de la logica
	 * 
	 * @param msg- el mensaje de la exepcion.
	 */
	public PrototipoExcepcion(String msg) {
		super(msg);
	}
}
