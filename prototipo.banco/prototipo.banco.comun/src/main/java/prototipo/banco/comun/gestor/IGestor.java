package prototipo.banco.comun.gestor;

import java.util.Date;

import prototipo.banco.comun.logica.Cliente;
import prototipo.banco.comun.logica.Cuenta;
import prototipo.banco.comun.logica.Movimiento;

/**
 * Es la interfaz que contiene los metodos necsarios para usar el gigaspaces
 * 
 * @author oscar.ospina
 *
 */
public interface IGestor {

	/**
	 * Crea un cliente en el espacio
	 * 
	 * @param cliente el cliente que se desea crear en el espacio.
	 */
	public void crearCliente(Cliente cliente);

	/**
	 * Crea una cuenta en el espacio.
	 * 
	 * @param cuenta es la cuenta que se desea crear en el espacio.
	 */
	public void crearCuenta(Cuenta cuenta);

	/**
	 * Busca los clientes del espacio.
	 * 
	 * @return un arreglo con los clientes del espacio
	 */
	public Cliente[] buscarClientes();

	/**
	 * Busca un cliente dado su numero de identificación
	 * 
	 * @param numeroIdCliente es el numero de identificación del cliente
	 * @return el cliente con el numero de identificación numeroIdCliente
	 */
	public Cliente buscarCliente(String numeroIdCliente);

	/**
	 * Busca las cuentas de un cliente
	 * 
	 * @param numeroIdCliente el numero de identificación del cliente
	 * @return las cuentas del cliente con el numero de identifacación
	 *         numeroIdCliente
	 */
	public Cuenta[] buscarCuentasCliente(String numeroIdCliente);

	/**
	 * Busca una cuenta dado su numero
	 * 
	 * @param numeroCuenta el numero con el cual se identifica a la cuenta
	 * @return la cuenta con el numero numeroCuenta
	 */
	public Cuenta buscarCuenta(Integer numeroCuenta);

	/**
	 * Crea un movimiento en el espacio
	 * 
	 * @param movimiento el movimiento que se desea crear en el espacio
	 */
	public void creaMovimiento(Movimiento movimiento);

	/**
	 * Elimina un cliente dado su numero de identifiación
	 * 
	 * @param numeroId el numero de identificación del cliente
	 */
	public void eliminarCliente(String numeroId);

	/**
	 * Buasca todas las cuentas que estan en el espacio.
	 * 
	 * @return una lista las cuentas del espacio.
	 */
	public Cuenta[] buscarCuentas();

	/**
	 * Busca los movimientos de una cuenta dado el numero de la cuenta
	 * 
	 * @param numeroCuenta el numero con el cual se identifica la cuenta
	 * @return una lista con los movimientos de una cuenta con numero
	 *         numeroCuenta
	 */
	public Movimiento[] buscarMovimientoCuentas(Integer numeroCuenta);

	/**
	 * Elimina una cuenta dado su numero
	 * 
	 * @param numero de identificación de la cuenta
	 */
	public void eliminarCuenta(Integer numero);

	/**
	 * Elimina un movimiento
	 * 
	 * @param movimiento el movimiento que se quiere eliminar
	 */
	public void eliminarMovimiento(Movimiento movimiento);

	/**
	 * Elimina los movimientos de una cuenta dado su numero
	 * 
	 * @param numero de identificación de la cuenta
	 */
	public void eliminarMovimientosCuenta(Integer numero);

	/**
	 * elimina las cuentas de un cliente dado el id del cliente
	 * 
	 * @param numeroId el numero de identifiacion del cliente
	 */
	public void eliminarCuentasCliente(String numeroId);

	/**
	 * Busca todos los movimientos del espacio
	 * 
	 * @return una lista de los movimientos
	 */
	public Movimiento[] buscarMovimientos();

	/**
	 * Busca los movimientos de una cuenta entre dos fechas
	 * 
	 * @param numeroCuenta el numero con el cual se identifica la cuenta
	 * @param fechaInicio la fecha inicial
	 * @param fechaFin la fecha final
	 * @return una lista con los movimientos.
	 */
	public Movimiento[] buscarMovimientoCuentas(Integer numeroCuenta, Date fechaInicio,
			Date fechaFin);
}
