package prototipo.banco.web.wicket;

import org.apache.wicket.markup.html.border.Border;
import org.apache.wicket.markup.html.border.BoxBorder;

/**
 * Clase que representa el borde de la barra de navegación.
 * 
 * @author oscar.ospina
 *
 */
public class Borde extends Border {

	/**
	 * el numero serial utilizado para serializar la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Crea un nuevo borde.
	 * 
	 * @param id el numero de identificación del borde.
	 */
	public Borde(final String id) {
		super(id);
		addToBorder(new BoxBorder("navigationBorder"));
	}
}