package prototipo.banco.procesador;

import org.openspaces.events.adapter.SpaceDataEvent;
import org.springframework.transaction.annotation.Transactional;

import prototipo.banco.comun.excepcion.PrototipoExcepcion;
import prototipo.banco.comun.gestor.Gestor;
import prototipo.banco.comun.gestor.IGestor;
import prototipo.banco.comun.logica.Cuenta;
import prototipo.banco.comun.logica.Movimiento;

/**
 * La unidad de procesamiento que esta encargada de agregar a los movimientos
 * 
 * @author oscar.ospina
 *
 */
public class Procesador {

	/**
	 * Gestor encargado de acceder al espacio
	 */
	private final IGestor gestor = Gestor.getInstance();

	/**
	 * Agrega a la cuenta un movimiento y actualiza los datos en el espacio.
	 * 
	 * @param movimiento el movimiento que se quiere agregar a la cuenta.
	 * @return El movimiento actualizado en el espacio.
	 */
	@SpaceDataEvent
	@Transactional
	public Movimiento procesarMovimiento(Movimiento movimiento) {
		Cuenta cuenta = gestor.buscarCuenta(movimiento.getCuenta().getNumero());
		movimiento.setProcesado(true);
		gestor.creaMovimiento(movimiento);
		try {
			cuenta.addMovimiento(movimiento);
		} catch (PrototipoExcepcion e) {
			e.printStackTrace();
		}
		gestor.crearCuenta(cuenta);
		return movimiento;
	}
}