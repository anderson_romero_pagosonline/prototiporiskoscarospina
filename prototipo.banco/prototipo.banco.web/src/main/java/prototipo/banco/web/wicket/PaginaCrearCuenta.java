package prototipo.banco.web.wicket;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.Model;

import prototipo.banco.comun.logica.Cliente;
import prototipo.banco.comun.logica.Cuenta;

/**
 * Representa la pagina en la cual se agrega una cuenta.
 * 
 * @author oscar.ospina
 *
 */
public class PaginaCrearCuenta extends PaginaBase {
	/**
	 * El numero serial utilizado para serializar la clase.
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Es la lista desplegable con los numeros de identificacion de los clientes.
	 */
	private DropDownChoice<String> numeroIdentificacion;
	/**
	 * El campo de texto del numero de la cuenta.
	 */
	private TextField<Integer> numero;
	/**
	 * El campo de texto del saldo de la cuenta.
	 */
	private TextField<BigDecimal> saldo;
	/**
	 * Las lista de las cedulas de los cleintes.
	 */
	private List<String> cedulas;
	/**
	 * El Form basico de la pagina.
	 */
	private Form<String> form;

	/**
	 * Constructor de la pagina.
	 */
	public PaginaCrearCuenta() {
		inicializarElementos();
		agregarElementos();
		hacerCamposObligatorios();
	}

	/**
	 * Vuelve los campos numeroIdentificacion, numero y saldo requeridos.
	 */
	public void hacerCamposObligatorios() {
		numeroIdentificacion.setRequired(true);
		numero.setRequired(true);
		saldo.setRequired(true);
	}

	/**
	 * Instancia todos los elementos de la pagina Web.
	 */
	public void inicializarElementos() {
		cedulas = new ArrayList<String>();
		Cliente[] clientes = getGestor().buscarClientes();
		cedulasClientes(clientes);
		numeroIdentificacion = new DropDownChoice<String>("numeroIdentificacion",
				new Model<String>(""), cedulas);
		numero = new TextField<Integer>("numero", new Model<Integer>(), Integer.class);
		saldo = new TextField<BigDecimal>("saldo", new Model<BigDecimal>(), BigDecimal.class);
		form = new Form<String>("form");
	}

	/**
	 * Agrega los diferentes elementos a la pagina.
	 */
	public void agregarElementos() {
		form.add(numeroIdentificacion);
		form.add(numero);
		form.add(saldo);
		form.add(new Button("crear") {
			private static final long serialVersionUID = 1L;

			@Override
			public void onSubmit() {
				crearCuenta(numeroIdentificacion.getModelObject(), numero.getModelObject(),
						saldo.getModelObject());
				setResponsePage(PaginaInicio.class);
			}
		});
		form.add(new Link<String>("cancelar") {
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				setResponsePage(PaginaInicio.class);
			}
		});
		add(form);
		add(new FeedbackPanel("feedback"));
	}

	/**
	 * Crea una cuenta nueva y la agrega en el espacio.
	 * 
	 * @param numeroIdCliente el numerode identificación del cliente.
	 * @param numeroCuenta numero con el cual se identifica la cuenta.
	 * @param saldoCuenta el saldo que tiene la cuenta.
	 */
	public void crearCuenta(String numeroIdCliente, Integer numeroCuenta, BigDecimal saldoCuenta) {
		Cliente cliente = getGestor().buscarCliente(numeroIdCliente);

		Cuenta cuenta = new Cuenta(cliente, numeroCuenta, saldoCuenta);
		getGestor().crearCuenta(cuenta);
		cliente.addCuenta(cuenta);
		getGestor().crearCliente(cliente);
	}

	/**
	 * Agrega los numero de identificación a la lista cedulas dado una lista de
	 * clientes.
	 * 
	 * @param clientes la lista de clientes, de los qcuales se obtiene los
	 *        numero de identificación.
	 */
	public void cedulasClientes(Cliente[] clientes) {
		for (int i = 0; i < clientes.length; i++) {
			cedulas.add(clientes[i].getNumeroDeIdentificacion());
		}
	}
}
